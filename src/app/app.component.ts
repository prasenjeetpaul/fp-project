import { Component } from '@angular/core';
import { trigger, transition, style, animate } from '@angular/animations';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  animations: [
    trigger('slide', [
      transition('left => right', [
        style({ opacity: 0, transform: 'translateX(-100%)' }),
        animate(250)
      ]),
      transition('right => left', [
        style({ opacity: 0, transform: 'translateX(100%)' }),
        animate(250)
      ]),
    ])
  ]
})
export class AppComponent {
  isSigninVisible: boolean;

  constructor() { }

  ngOnInit() {
    this.isSigninVisible = true;
  }

  toggleVisibility() {
    this.isSigninVisible = !this.isSigninVisible;
  }
}
